import math
def sqroot(a,b,c):
    #delta
    delta=b*b-4*a*c
    if delta==0:
        return -b/(2*a)
    elif delta>0:
        return [-b+math.sqrt(delta)/(2*a),-b-math.sqrt(delta)/(2*a)]

def horner(P,x):
    R=len(P)*[type(x)]
    for deg,coeff in enumerate(P):
        if deg==0: R[-deg]= coeff
        else: R[-deg]=R[-deg+1]*x+coeff
    
    return R





    
