import math as m
import sys

def polydiv(M,N): #divide polynomial lists and return [polynomial , rest] list
    degM = len(M)-1
    degN = len(N)-1
    Q =[None]*(degM-degN+1)
    multiplier=None
    #copying the M polynomial
    M_loc = [elem for elem in M]
    
    if degM >= degN:
        exponent = degM-degN
        #dealing with for relative primes
        if gcd(M_loc[-1],N[-1]) in range(1,10):
            multiplier = ( m.pow(N[-1],degM))
            if multiplier==m.floor(multiplier): multiplier=int(multiplier)
            M_loc=[multiplier*elem for elem in M_loc]
        while exponent>=0:
            arg=M_loc[-1]/N[-1]
            Q[exponent]=arg
            for index in range(-1,-len(N)-1,-1):
                M_loc[index] -= N[index]*arg
            del M_loc[-1]
            degM = len(M_loc)-1
            exponent = degM-degN         
        rest=M_loc
    else: 
        print("Wrong polynomial degrees!")
        return None

    return [Q, rest, multiplier]
  
def interpret(p): #polynomial list to human readable string
    p_string = str()
    for i in range(len(p)-1,-1,-1): 
        if p[i] != 0:
            if p[i]==m.floor(p[i]):
                p[i] = int(p[i])    #convert for example 4.0 -> 4
                coeff = str(p[i])
            else:
                coeff=getRatio(p[i]) 

            exp = str(i)
            if coeff != '1' and coeff != '-1':
                if i>1:     p_string+=coeff+ "x^" + exp  
                elif i==1:  p_string+=coeff + "x" 
            else:
                if coeff=='-1' and i!=0:p_string+='-'
                if i>1:     p_string+="x^" + exp  
                elif i==1:  p_string+="x"

            if i==0:  p_string+=coeff

        if p[i-1]>0 and i-1>=0: p_string+='+'

    return p_string

def getRatio(f): #convert float to a ratio string "a/b"
    r = f.as_integer_ratio()
    return "{}/{}".format(r[0],r[1])

def gcd(a,b): #greatest common divisor (NWD)
    a = abs(a)
    b = abs(b)
    while a!=b :
            if a>b : a-=b
            else: b-=a
    return a

def verify(M,N,Q,rest,multi):
    local = [0.0]*(len(M))
    #multiply N*Q
    for idx,n in enumerate(N):
        for idy,q in enumerate(Q):
            local[idx+idy]+=n*q
    #add rest
    for idx,r in enumerate(rest):
        local[idx]+=r
    #compare
    M_multi = [m*multi for m in M]
    if M_multi == local: return True
    else: return False

#================================================================================================================================

if len(sys.argv)<2:
    #interactive input
    print("Input a polynomial to be divided, coefficients separated by a space")
    in_string1 = input()
    print("Similarly input the one to be divided by")
    in_string2 = input()
    div_char=' '
else:
    #console input
    #print(sys.argv)
    in_string1 = sys.argv[1]
    in_string2 = sys.argv[2]
    div_char=' '
    

try:
    poly1 = [float(i) for i in in_string1.split(div_char)]
    poly2 = [float(i) for i in in_string2.split(div_char)]
    poly1.reverse()
    poly2.reverse()
except(ValueError):
    print("Wrong input!")
    exit(0)

divided = polydiv(poly1,poly2)
if not verify(poly1,poly2,divided[0],divided[1],divided[2]): print("Error ocurred")
#print(divided)
if divided is not None:
    if divided[2] in {None,1}:print("{} = ({})*({}) + ({})".format( interpret(poly1),interpret(poly2),interpret(divided[0]),interpret(divided[1])) )
    else: print("{0} = ({1})*({2})/{4} + ({3})/{4}".format(interpret(poly1),interpret(poly2),interpret(divided[0]),interpret(divided[1]),divided[2]) )


